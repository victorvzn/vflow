#!/bin/bash

rm -rf .git
rm .gitignore
rm index.html
rm README.md

echo ">>> INIT =============================="
bash ../vflow.sh init
echo ""
echo ""

echo ">>> ADD REMOTE ORIGIN =============================="
git remote add origin https://gitlab.com/victorvzn/vflow.git
echo ""
echo ""

echo ">>> FIRST COMMIT =============================="
printf "# TEST VFLOW" > README.md
printf "*.sh" > .gitignore
git add README.md
git add .gitignore
git commit -m "Add README.md and .gitignore"
git push -u origin master
echo ""
echo ""

echo ">>> CONFIG =============================="
bash ../vflow.sh config
echo ""
echo ""

echo ">>> FEATURE 1 =============================="
bash ../vflow.sh fs hello
printf "<html><body><h1>Hello</h1></body></html>" > index.html
git add index.html
git commit -m "Add new page with hello (F1)"
bash ../vflow.sh ff hello
echo ""
echo ""

echo ">>> FEATURE 2 =============================="
bash ../vflow.sh fs world
printf "<html><body><h1>Hello world</h1></body></html>" > index.html
git add index.html
git commit -m "Add new page with world (F2)"
bash ../vflow.sh ff world
echo ""
echo ""

echo ">>> RELEASE 1 =============================="
bash ../vflow.sh r v1.1.0 "Release-1"
echo ""
echo ""

echo ">>> FEATURE 3 =============================="
bash ../vflow.sh fs text-right
printf "<html><body><h1 style=\"text-align:right;\">Hello world</h1></body></html>" > index.html
git add index.html
git commit -m "Text align right (F3)"
bash ../vflow.sh ff text-right
echo ""
echo ""

echo ">>> RELEASE 2 =============================="
bash ../vflow.sh r v1.2.0 "Release-2"
echo ""
echo ""

echo ">>> HOTFIX 1 =============================="
bash ../vflow.sh hs text-center
printf "<html><body><h1 style=\"text-align:center;\">Hello world</h1></body></html>" > index.html
git add index.html
git commit -m "Text align center"
bash ../vflow.sh hf text-center v1.2.1 "Hotfix-1"
echo ""
echo ""

echo ">>> FEATURE 4 =============================="
bash ../vflow.sh fs new-line
printf "<html><body><h1 style=\"text-align:center;\">Hello world</h1><h2>New line</h2></body></html>" > index.html
git add index.html
git commit -m "New line (F4)"
bash ../vflow.sh ff new-line
echo ""
echo ""

echo ">>> FEATURE 5 =============================="
bash ../vflow.sh fs text-orange
printf "<html><body><h1 style=\"text-align:center;color:orange;\">Hello world</h1><h2>New line</h2></body></html>" > index.html
git add index.html
git commit -m "Text orange (F5)"
bash ../vflow.sh ff text-orange
echo ""
echo ""

echo ">>> FEATURE 6 =============================="
bash ../vflow.sh fs text-blue
printf "<html><body><h1 style=\"text-align:center;color:blue;\">Hello world</h1><h2>New line</h2></body></html>" > index.html
git add index.html
git commit -m "Text blue (F6)"
bash ../vflow.sh ff text-blue
echo ""
echo ""

echo ">>> FEATURE 7 =============================="
bash ../vflow.sh fs text-yellow
printf "<html><body><h1 style=\"text-align:center;color:yellow;\">Hello world</h1><h2>New line</h2></body></html>" > index.html
git add index.html
git commit -m "Text yellow (F7)"
bash ../vflow.sh ff text-yellow
echo ""
echo ""

echo ">>> RELEASE 3 =============================="
bash ../vflow.sh r v1.3.0 "Release-3"
echo ""
echo ""

echo ">>> HOTFIX 2 =============================="
bash ../vflow.sh hs text-green
printf "<html><body><h1 style=\"text-align:center;color:green;\">Hello world</h1><h2>New line</h2></body></html>" > index.html
git add index.html
git commit -m "Text green"
bash ../vflow.sh hf text-green v1.3.1 "Hotfix-2"
echo ""
echo ""

echo ">>> FEATURE 8 =============================="
bash ../vflow.sh fs text-exclamation
printf "<html><body><h1 style=\"text-align:center;color:green;\">Hello world!</h1><h2>New line</h2></body></html>" > index.html
git add index.html
git commit -m "Text exclamation (F8)"
bash ../vflow.sh ff text-exclamation
echo ""
echo ""

echo ">>> FEATURE 9 =============================="
bash ../vflow.sh fs header2-center
printf "<html><body><h1 style=\"text-align:center;color:green;\">Hello world!</h1><h2 style=\"text-align:center;\">New line</h2></body></html>" > index.html
git add index.html
git commit -m "header2-center (F9)"
bash ../vflow.sh ff header2-center
echo ""
echo ""

git la