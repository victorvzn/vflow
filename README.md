# Welcome to my git flow

```
$ bash vflow.sh

vflow cli 0.1.0
================

Available commands:

    init
    config

    feature start [branch_name]
    feature finish [branch_name]

    release [tag_name] [message]

    hotfix start [branch_name]
    hotfix finish [branch_name] [tag_name] [message]
```


### How to work?

## 1. Create a project

```
      () $ git init
(master) $ touch README.md
(master) $ git add -A
(master) $ git commit -m "Adding README.md [master]"

```

## 2. Create a feature branch

```
(master) $ git checkout master
(master) $ git checkout -b feature-hello
```

### 2.1. Do commits

```
(feature-hello) $ touch index.html < '<html><body><h1>Hello world</h1></body></html>'
(feature-hello) $ git add -A
(feature-hello) $ git commit -m "Adding website info in index.html [feature-hello]"
```

### 2.2. Merge

```
(master) $ git checkout master
(master) $ git merge --no-ff feature-hello
(master) $ git branch -d feature-hello
(master) $ git push origin master
```

## 3. Deploy 01

```
(master) $ git checkout -b production
(production) $ git merge master
(production) $ git tag -a v1.1.0 -m "First release"
(production) $ git push --tags origin production
(production) $ git checkout master
```

## 4. Create a feature branch

```
(master) $ git checkout -b feature-aligning-header
```

### 4.1. Do commits

```
(feature-aligning-header) $ git add -A
(feature-aligning-header) $ git commit -m "Aligning header to the right in index.html [feature-aligning-header]"
(feature-aligning-header) $ touch index.html < '<html><body><h1 style="text-align:right;">Hello world</h1></body></html>'
```

### 4.2. Merge

```
(master) $ git checkout master
(master) $ git merge --no-ff feature-aligning-header
(master) $ git branch -d feature-aligning-header
```

## 5. Deploy

```
(master) $ git checkout -b production
(production) $ git merge master
(production) $ git tag -a v1.2.0 -m "Second release"
(production) $ git push --tags origin production
(production) $ git checkout master
```

## 6. Fixing problems

```
(master) $ git checkout production
(production) $ git checkout -b hotfix
```

### 6.1. Do commits

```
(hostfix) $ touch index.html < '<html><body><h1 style="text-align:center;">Hello world</h1></body></html>'
(hostfix) $ git add -A
(hostfix) $ git commit -m "Centering header in index.html [hostfix]"
```

### 6.2. Merge

```
(production) $ git checkout production
(production) $ git merge --no-ff hotfix
(production) $ git tag -a v1.2.1 -m "First hotfix" 
(production) $ git branch -d hotfix
```
### 6.3. Integrate production and master

```
(production) $ git checkout master
(master) $ git merge production 
```

## 7. Rewind/forward version

```
(production) git checkout v1.2.0
```

* Source: https://es.slideshare.net/viniciusban/gitlab-flow-solo-39625228
