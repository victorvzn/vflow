#!/bin/bash

validate()
{
  if [ -z "$2" ];
  then
    echo $"[WARNING] Parameter $1 is invalid"
    exit 0
  fi
}

if [ -z "$1" ]
then
    echo $""
    echo $"vflow cli 0.1.0"
    echo $"================"
    echo $""
    echo $"Available commands:"
    echo $""
    echo $"    init"
    echo $"    config"
    echo $""
    echo $"    feature start [branch_name]"
    echo $"    feature finish [branch_name]"
    echo $""
    echo $"    release [tag_name] [message]"
    echo $""
    echo $"    hotfix start [branch_name]"
    echo $"    hotfix finish [branch_name] [tag_name] [message]"
    echo $""
else
  case "$1" in
    "init"|i)
            git init
            ;;
    "config"|c)
            git branch production > /dev/null 2>&1
            ;;
    "feature start"|fs)
            validate BRANCH_NAME $2
            git checkout master > /dev/null 2>&1
            git pull origin master > /dev/null 2>&1
            git checkout -b feature/$2 > /dev/null 2>&1
            ;;
    "feature finish"|ff)
            validate branchANCH_NAME $2
            git checkout master > /dev/null 2>&1
            git merge --no-ff feature/$2 > /dev/null 2>&1
            git branch -d feature/$2 > /dev/null 2>&1
            git push -u origin master > /dev/null 2>&1
            ;;
    "release"|r)
            validate TAG_NAME $2
            # git checkout master > /dev/null 2>&1
            #  git pull origin production > /dev/null 2>&1
            git checkout production > /dev/null 2>&1
            git merge --no-ff  master > /dev/null 2>&1
            git tag -a $2 -m "$3" > /dev/null 2>&1
            git push --tags origin production > /dev/null 2>&1
            ;;
     "hotfix start"|hs)
            validate BRANCH_NAME $2 > /dev/null 2>&1
            git checkout production > /dev/null 2>&1
            git checkout -b hotfix/$2 > /dev/null 2>&1
            ;;
    "hotfix finish"|hf)
            validate BRANCH_NAME $2
            validate TAG_NAME $3
            git checkout production > /dev/null 2>&1
            git merge --no-ff hotfix/$2 > /dev/null 2>&1
            git branch -d hotfix/$2 > /dev/null 2>&1
            git tag -a $3 -m "$4" > /dev/null 2>&1
            git checkout master > /dev/null 2>&1
            git merge --no-ff production > /dev/null 2>&1
            git push --tags origin production > /dev/null 2>&1
            ;;
  *) echo $"The Parameters are incorrects!"
       ;;
  esac
fi
